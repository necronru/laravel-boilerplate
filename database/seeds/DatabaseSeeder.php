<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Laracasts\TestDummy\Factory as TestDummy;

use SleepingOwl\AdminAuth\Entities\Administrator;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		// $this->call('UserTableSeeder');

		TestDummy::create('App\User', [
			'email' => 'user@example.com'
		]);
		TestDummy::times(10)->create('App\User');

		Administrator::create([
			'username' 	=> 'admin',
			'password' 	=> '123456',
			'name' 		=> 'Админ'
		]);
	}

}
