<?php

$model = Admin::model('App\User');

if ( ! $user = Auth::user() )
    return;

if ( ! $user->hasRole('admin') )
    return;

$model->title(Lang::choice('messages.users', 1))->display(function ()
{
	$display = AdminDisplay::table();
	$display->columns([
		Column::string('name')->label('Name'),
		Column::string('email')->label('Email'),
	]);
	return $display;
})->createAndEdit(function ($id)
{
	$form = AdminForm::form();
	$form->items([
		FormItem::text('name', Lang::choice('messages.name', 1))->required(),
		FormItem::text('email', Lang::choice('messages.email', 1))->required()->unique(),
        FormItem::custom()->display(function ($instance)
        {
            return view('admin._partials.form.password', [
                'instance' => $instance,
                'name' => 'password',
                'type' => 'password',
                'title' => Lang::choice('messages.password', 1)
            ]);

        })->callback(function ($instance) use ($id)
        {
            $password    = Input::get('password');

            if (!$id) {
                $instance->password = bcrypt($password);
                $instance->save();
                return;
            }


            $oldInstance = App\User::find($instance->id);

            /** @var App\User $oldInstance */
            if ($oldInstance->password === $password) return;

            $instance->password = bcrypt($password);
            $instance->save();

        })
	]);
	return $form;
});