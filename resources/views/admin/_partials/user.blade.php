<ul class="nav navbar-top-links navbar-right">
    <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
            <i class="fa fa-user fa-fw"></i> {{ Auth::user()->name ?: 'admin' }} ({{ Auth::user()->roles()->first()->display_name ?: '' }}) <i class="fa fa-caret-down"></i>
        </a>
        <ul class="dropdown-menu dropdown-user">
            <li><a href="{{ url('auth/logout') }}"><i class="fa fa-sign-out fa-fw"></i> {{ trans('admin::lang.auth.logout') }}</a></li>
        </ul>
    </li>
</ul>
