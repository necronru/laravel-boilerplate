<?php

$admin = Auth::user();

Admin::menu()->url('/')->label(Lang::trans('messages.dashboard'))->icon('fa-dashboard');

if ($admin && $admin->hasRole('admin')) {
    Admin::menu()->label(Lang::choice('messages.settings', 1))->icon('fa-gear')->items(function() {
        Admin::menu()->url('users')->label(Lang::choice('messages.users', 1))->icon('fa-user');
        Admin::menu()->url('roles')->label(Lang::choice('messages.roles', 1))->icon('fa-group');

    });
}

